# FoodSupply

Projeto Integrador - Unimar

O FoodSupply é um software desenvolvido com intuito de ajudar empreendedores a encontrarem fornecedores de forma fácil e ágil. 
O frontend foi desevolvido utilizando o framework ReactJS e bibliotecas como Material UI e SweetAlert para personalização das telas e componentes.

## Dependências

* NodeJS

## Como executar 

* npm install / yarn install

* npm run start

## Desenvolvedores

* Vitor Zafra - https://github.com/vitorzf
* Rian Imafuku - https://github.com/RianImafuku
* Jonatan Rocha - https://github.com/jonatanfrc
